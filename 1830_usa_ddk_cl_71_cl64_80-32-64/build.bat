@echo off

::set CFLAGS=/Zi %CFLAGS%
::set LFLAGS=/subsystem:console /debug %CFLAGS%
set CFLAGS=/DDRIVE=C %CFLAGS%

setlocal

fsutil hardlink create lnprog.c ..\lnprog.c>nul 2>&1
fsutil hardlink create ntdll_stub.c ..\ntdll_stub.c>nul 2>&1
fsutil hardlink create ntdll_stub.def ..\ntdll_stub.def>nul 2>&1
del *.pdb>nul 2>&1

:: for editbin
set PATH=C:\PROGRA~1\MICROS~2\Common\msdev98\BIN;C:\PROGRA~1\MICROS~2\VC98\BIN;%PATH%
set PATH=G:\1830_usa_ddk\bin\x86;%PATH%


call ..\ntdll_stub_create_lib.bat X86|| goto :err
move ntdll.lib ntdll32.lib>nul

set CFLAGS=/O1 %CFLAGS%
::set LFLAGS=/subsystem:native /fixed /safeseh:no /merge:.rdata=.data %LFLAGS%
set LFLAGS=/subsystem:native,5.1 /fixed /safeseh:no /opt:ref /merge:.rdata=.flat /merge:.data=.flat %LFLAGS%
set LIBS=ntdll32.lib %LIBS%
set EDITBIN=/nologo /section:.text=.flat,w lnprog32.obj
call ..\build.bat 32 noflags|| goto :err


endlocal
setlocal

:: for editbin
set PATH=G:\MSVS2005\VC\bin\x86_amd64;%PATH%
set PATH=G:\1830_usa_ddk\bin\win64\x86\amd64;%PATH%

call ..\ntdll_stub_create_lib.bat AMD64|| goto :err
move ntdll.lib ntdll64.lib>nul

set CFLAGS=/GS- /O1 %CFLAGS%
::set LFLAGS=/subsystem:native /fixed /safeseh:no /merge:.rdata=.data %LFLAGS%
set LFLAGS=/subsystem:native,5.2 /fixed /safeseh:no /opt:ref /merge:.rdata=.flat /merge:.data=.flat %LFLAGS%
set LIBS=ntdll64.lib %LIBS%
set EDITBIN=/nologo /section:.pdata,m /section:.text=.flat,w lnprog64.obj
call ..\build.bat 64 noflags|| goto :err


del lnprog.c
del ntdll_stub.c
del ntdll_stub.def
del vc*.pdb>nul 2>&1

endlocal
exit /b

:err
endlocal
exit /b 1
