@echo off
set PROMPT=^>
setlocal

if "%2"=="debug" (
	set CFLAGS=/Od /Zi %CFLAGS%
	set LFLAGS=/subsystem:console /debug %LFLAGS%
	set LIBS=ntdll.lib %LIBS%
) else if "%2"=="release" (
	set CFLAGS=/Od %CFLAGS%
	set LFLAGS=/subsystem:native %LFLAGS%
	set LIBS=ntdll.lib %LIBS%
) else if "%2"=="release_cl14" (
	set CFLAGS=/Od /GS- %CFLAGS%
	set LFLAGS=/subsystem:native /fixed /safeseh:no /merge:.rdata=.data %LFLAGS%
) else if "%2"=="debug_cl14" (
	set CFLAGS=/Od /GS- /Zi %CFLAGS%
	set LFLAGS=/subsystem:console /debug %LFLAGS%
) else if "%2"=="noflags" (
	rem
)

if "%1"=="32" goto :32
if "%1"=="64" goto :64
goto :err

:32
	echo on
	cl /nologo %CFLAGS% /Gz /DTARGET=32 /c /W4 /Fo"lnprog32.obj" lnprog.c || goto :err
	if not "%EDITBIN%"=="" editbin %EDITBIN% || goto :err
	link /nologo %LFLAGS% /nodefaultlib /entry:entry /opt:nowin98 /incremental:no /out:lnprog32.exe lnprog32.obj %LIBS% || goto :err
	@echo off
	goto exit

:64
	echo on
	cl /nologo %CFLAGS% /Gz /DTARGET=64 /c /W4 /Fo"lnprog64.obj" lnprog.c || goto :err
	if not "%EDITBIN%"=="" editbin %EDITBIN% || goto :err
	link /nologo %LFLAGS% /nodefaultlib /entry:entry /opt:nowin98 /incremental:no /out:lnprog64.exe lnprog64.obj %LIBS% || goto :err
	@echo off
	goto exit
:exit

endlocal
exit /b

:err
endlocal
exit /b 1
