//#include <windows.h>
#pragma warning(disable : 4201) // nameless struct/union
//#include <winioctl.h>

//#if defined(_WIN64)
//#elif defined(_WIN32)
typedef unsigned int DWORD;
typedef unsigned short WORD;
typedef unsigned char BYTE;
typedef short WCHAR;
typedef __int64 LONGLONG;
typedef unsigned __int64 ULONGLONG;
typedef unsigned long ULONG_PTR;
//#endif
//#if defined(_WIN64)
// typedef unsigned __int64 ULONG_PTR;
//#else
//#endif


typedef long LONG;
typedef unsigned long ULONG;
typedef unsigned short USHORT;
typedef WCHAR *LPWSTR, *PWSTR;
typedef BYTE  BOOLEAN;

typedef void *PVOID, *LPVOID;
typedef PVOID HANDLE, *PHANDLE;
typedef LONG NTSTATUS;
typedef DWORD ACCESS_MASK;

#define NTSYSAPI DECLSPEC_IMPORT
#define DECLSPEC_IMPORT __declspec(dllimport)
#define NTAPI __stdcall

#define NULL ((PVOID)0)

#define FALSE               0
#define TRUE                1

#define IO_REPARSE_TAG_MOUNT_POINT               (0xA0000003)


typedef union _LARGE_INTEGER {
    struct {
        DWORD LowPart;
        LONG HighPart;
    };
    struct {
        DWORD LowPart;
        LONG HighPart;
    } u;
    LONGLONG QuadPart;
} LARGE_INTEGER;

typedef LARGE_INTEGER *PLARGE_INTEGER;



#define FILE_ATTRIBUTE_READONLY             0x00000001  
#define FILE_ATTRIBUTE_HIDDEN               0x00000002  
#define FILE_ATTRIBUTE_SYSTEM               0x00000004  
#define FILE_ATTRIBUTE_DIRECTORY            0x00000010  
#define FILE_ATTRIBUTE_ARCHIVE              0x00000020  
#define FILE_ATTRIBUTE_ENCRYPTED            0x00000040  
#define FILE_ATTRIBUTE_NORMAL               0x00000080  
#define FILE_ATTRIBUTE_TEMPORARY            0x00000100  
#define FILE_ATTRIBUTE_SPARSE_FILE          0x00000200  
#define FILE_ATTRIBUTE_REPARSE_POINT        0x00000400  
#define FILE_ATTRIBUTE_COMPRESSED           0x00000800  
#define FILE_ATTRIBUTE_OFFLINE              0x00001000  
#define FILE_ATTRIBUTE_NOT_CONTENT_INDEXED  0x00002000  


#define CTL_CODE( DeviceType, Function, Method, Access ) (                 \
    ((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method) \
)

#define FILE_DEVICE_FILE_SYSTEM         0x00000009
#define FILE_ANY_ACCESS                 0
#define METHOD_BUFFERED                 0



//
// Structures for FSCTL_SET_REPARSE_POINT, FSCTL_GET_REPARSE_POINT, and FSCTL_DELETE_REPARSE_POINT
//

//
// The reparse structure is used by layered drivers to store data in a
// reparse point. The constraints on reparse tags are defined below.
// This version of the reparse data buffer is only for Microsoft tags.
//

typedef struct _REPARSE_DATA_BUFFER {
    DWORD  ReparseTag;
    WORD   ReparseDataLength;
    WORD   Reserved;
    union {
        struct {
            WORD   SubstituteNameOffset;
            WORD   SubstituteNameLength;
            WORD   PrintNameOffset;
            WORD   PrintNameLength;
            WCHAR PathBuffer[1];
        } SymbolicLinkReparseBuffer;
        struct {
            WORD   SubstituteNameOffset;
            WORD   SubstituteNameLength;
            WORD   PrintNameOffset;
            WORD   PrintNameLength;
            WCHAR PathBuffer[1];
        } MountPointReparseBuffer;
        struct {
            BYTE   DataBuffer[1];
        } GenericReparseBuffer;
    };
} REPARSE_DATA_BUFFER, *PREPARSE_DATA_BUFFER;

#define REPARSE_DATA_BUFFER_HEADER_SIZE   FIELD_OFFSET(REPARSE_DATA_BUFFER, GenericReparseBuffer)













typedef struct _LSA_UNICODE_STRING {
	USHORT Length;
	USHORT MaximumLength;
	PWSTR  Buffer;
} LSA_UNICODE_STRING, *PLSA_UNICODE_STRING, UNICODE_STRING, *PUNICODE_STRING;
typedef struct _LSA_UNICODE_STRING const *PCUNICODE_STRING;

typedef struct _OBJECT_ATTRIBUTES {
	ULONG           Length;
	HANDLE          RootDirectory;
	PUNICODE_STRING ObjectName;
	ULONG           Attributes;
	PVOID           SecurityDescriptor;
	PVOID           SecurityQualityOfService;
} OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;

typedef struct _IO_STATUS_BLOCK {
	union {
		NTSTATUS Status;
		PVOID    Pointer;
	};
	ULONG_PTR Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;


NTSYSAPI NTSTATUS NTAPI NtCreateFile(
	PHANDLE            FileHandle,        // _Out_
	ACCESS_MASK        DesiredAccess,     // _In_
	POBJECT_ATTRIBUTES ObjectAttributes,  // _In_
	PIO_STATUS_BLOCK   IoStatusBlock,     // _Out_
	PLARGE_INTEGER     AllocationSize,    // _In_opt_
	ULONG              FileAttributes,    // _In_
	ULONG              ShareAccess,       // _In_
	ULONG              CreateDisposition, // _In_
	ULONG              CreateOptions,     // _In_
	PVOID              EaBuffer,          // _In_
	ULONG              EaLength           // _In_
);

NTSYSAPI NTSTATUS NTAPI NtOpenFile(
	PHANDLE            FileHandle,        // _Out_
	ACCESS_MASK        DesiredAccess,     // _In_
	POBJECT_ATTRIBUTES ObjectAttributes,  // _In_
	PIO_STATUS_BLOCK   IoStatusBlock,     // _Out_
	ULONG              ShareAccess,       // _In_
	ULONG              OpenOptions        // _In_
);

NTSYSAPI NTSTATUS NTAPI NtFsControlFile(
	HANDLE           FileHandle,          // _In_
	HANDLE           Event,               // _In_opt_
	PVOID            ApcRoutine,          // _In_opt_   // PIO_APC_ROUTINE  ApcRoutine
	PVOID            ApcContext,          // _In_opt_
	PIO_STATUS_BLOCK IoStatusBlock,       // _Out_
	ULONG            FsControlCode,       // _In_
	PVOID            InputBuffer,         // _In_opt_
	ULONG            InputBufferLength,   // _In_
	PVOID            OutputBuffer,        // _Out_opt_
	ULONG            OutputBufferLength   // _In_
);

NTSYSAPI NTSTATUS NTAPI NtSetInformationFile(
	HANDLE           FileHandle,
	PIO_STATUS_BLOCK IoStatusBlock,
	PVOID            FileInformation,
	ULONG            Length,
	ULONG            FileInformationClass //FILE_INFORMATION_CLASS FileInformationClass
);

NTSYSAPI BOOLEAN NTAPI RtlEqualUnicodeString(
	PCUNICODE_STRING String1,
	PCUNICODE_STRING String2,
	BOOLEAN          CaseInsensitive
);

NTSYSAPI NTSTATUS NTAPI NtClose(HANDLE);


#define STATUS_SUCCESS 0

/*
static NTSTATUS create_dir(POBJECT_ATTRIBUTES dir) {
  NTSTATUS status;
  HANDLE hndl;
  IO_STATUS_BLOCK iosb;

	status = NtCreateFile(
		&hndl,
		0x00100000, // SYNCHRONIZE
		dir,
		&iosb,
		NULL,
		FILE_ATTRIBUTE_NORMAL, // 0x00000080
		3, // FILE_SHARE_READ | FILE_SHARE_WRITE
		2, // FILE_CREATE
		1, // FILE_DIRECTORY_FILE
		NULL,
		0
	);
	if (status != STATUS_SUCCESS) return status;

	status = NtClose(hndl);
	return status;
}
*/



static char info_deletefile = 1;

static NTSTATUS create_link(POBJECT_ATTRIBUTES link, PVOID target, ULONG target_size) {
  NTSTATUS status;
  HANDLE hndl;
  IO_STATUS_BLOCK iosb;

	status = NtCreateFile(
		&hndl,
		0x00110002, // FILE_WRITE_DATA/FILE_ADD_FILE | DELETE | SYNCHRONIZE
		link,
		&iosb,
		NULL,
		FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM,
		0, // no share
		2, // FILE_CREATE
		0x00200001, // FILE_DIRECTORY_FILE | FILE_OPEN_REPARSE_POINT
		NULL,
		0
	);
	if (status != STATUS_SUCCESS) return status;

	status = NtFsControlFile(
		hndl,
		NULL,
		NULL,
		NULL,
		&iosb,
		CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 41, METHOD_BUFFERED, FILE_ANY_ACCESS), // FSCTL_SET_REPARSE_POINT // 0x000900a4
		target,
		target_size,
		NULL,
		0
	);

	if (status != STATUS_SUCCESS) {
		NtSetInformationFile(hndl, &iosb, &info_deletefile, 1, 0x0000000d); // delete empty directory
	}

	NtClose(hndl);
	return status;
}

static NTSTATUS set_link(POBJECT_ATTRIBUTES link, PVOID target, ULONG target_size) {
  NTSTATUS status;
  HANDLE hndl;
  IO_STATUS_BLOCK iosb;

	status = NtOpenFile(
		&hndl,
		0x00100002, // FILE_WRITE_DATA/FILE_ADD_FILE | SYNCHRONIZE
		link,
		&iosb,
		0x00000007, // FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE
		0x00200020  // FILE_OPEN_REPARSE_POINT | FILE_SYNCHRONOUS_IO_NONALERT
	);
	if (status != STATUS_SUCCESS) return status;

	status = NtFsControlFile(
		hndl,
		NULL,
		NULL,
		NULL,
		&iosb,
		CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 41, METHOD_BUFFERED, FILE_ANY_ACCESS), // FSCTL_SET_REPARSE_POINT // 0x000900a4
		target,
		target_size,
		NULL,
		0
	);

	NtClose(hndl);
	return status;
}

static NTSTATUS read_link(POBJECT_ATTRIBUTES link, PVOID target, ULONG max_target_size) {
  NTSTATUS status;
  HANDLE hndl;
  IO_STATUS_BLOCK iosb;

	status = NtOpenFile(
		&hndl,
		0x00100001, // FILE_READ_DATA/FILE_LIST_DIRECTORY | SYNCHRONIZE
		link,
		&iosb,
		0x00000007, // FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE
		0x00200020  // FILE_OPEN_REPARSE_POINT | FILE_SYNCHRONOUS_IO_NONALERT
	);
	if (status != STATUS_SUCCESS) return status;

	status = NtFsControlFile(
		hndl,
		NULL,
		NULL,
		NULL,
		&iosb,
		CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 42, METHOD_BUFFERED, FILE_ANY_ACCESS), // FSCTL_GET_REPARSE_POINT // 0x000900a8
		NULL,
		0,
		target,
		max_target_size
	);

	NtClose(hndl);
	return status;
}

static NTSTATUS delete_file(POBJECT_ATTRIBUTES file) {
  NTSTATUS status;
  HANDLE hndl;
  IO_STATUS_BLOCK iosb;

	status = NtOpenFile(
		&hndl,
		0x00110000, // DELETE | SYNCHRONIZE
		file,
		&iosb,
		0x00000007, // FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE
		0x00200000  // FILE_OPEN_REPARSE_POINT
	);
	if (status != STATUS_SUCCESS) return status;

	status = NtSetInformationFile(
		hndl,
		&iosb,
		&info_deletefile,
		1,
		0x0000000d // FileDispositionInformation
	);

	NtClose(hndl);
	return status;
}

static BOOLEAN are_links_equal(PREPARSE_DATA_BUFFER a, PREPARSE_DATA_BUFFER b) {
  UNICODE_STRING sa;
  UNICODE_STRING sb;
	if (a->ReparseTag != b->ReparseTag) {
		return FALSE;
	}

	sa.Length = sa.MaximumLength = a->MountPointReparseBuffer.SubstituteNameLength;
	sa.Buffer = (PWSTR)((char*)a->MountPointReparseBuffer.PathBuffer + a->MountPointReparseBuffer.SubstituteNameOffset);
	sb.Length = sb.MaximumLength = b->MountPointReparseBuffer.SubstituteNameLength;
	sb.Buffer = (PWSTR)((char*)b->MountPointReparseBuffer.PathBuffer + b->MountPointReparseBuffer.SubstituteNameOffset);
	return RtlEqualUnicodeString(&sa, &sb, TRUE); // case insensitive
}


struct REPARSE_DATA_BUFFER_HEADER {
	DWORD  ReparseTag;
	WORD   ReparseDataLength;
	WORD   Reserved;
};

struct MOUNTPOINT_REPARSE_BUFFER_HEADER {
	WORD   SubstituteNameOffset;
	WORD   SubstituteNameLength;
	WORD   PrintNameOffset;
	WORD   PrintNameLength;
};

//#define DRIVE_LETTER L"C"
#define DRIVE_LETTER__(x) L#x
#define DRIVE_LETTER_(x) DRIVE_LETTER__(x)
#define DRIVE_LETTER DRIVE_LETTER_(DRIVE)

#if TARGET==32

#define PATH_PROGS32 DRIVE_LETTER L":\\WINXP32\\Progs"
#define PATH_PROGS32_LENGTH ((sizeof(PATH_PROGS32)-1)/2)
static struct {
	struct REPARSE_DATA_BUFFER_HEADER header;
	struct MOUNTPOINT_REPARSE_BUFFER_HEADER mount_header;
	WCHAR buffer[PATH_PROGS32_LENGTH+1+4];
	WCHAR buffer2[PATH_PROGS32_LENGTH+1];
} link_to_progs32 = {
	{
		IO_REPARSE_TAG_MOUNT_POINT,
		sizeof(struct MOUNTPOINT_REPARSE_BUFFER_HEADER) + (PATH_PROGS32_LENGTH+4+1)*2 + (PATH_PROGS32_LENGTH+1)*2,
		0
	},
	{
		0, (PATH_PROGS32_LENGTH+4)*2, (PATH_PROGS32_LENGTH+1+4)*2, PATH_PROGS32_LENGTH*2
	},
	L"\\??\\" PATH_PROGS32,
	PATH_PROGS32
};

#elif TARGET==64

#define PATH_PROGS64_64 DRIVE_LETTER L":\\WINXP64\\Progs64"
#define PATH_PROGS64_64_LENGTH ((sizeof(PATH_PROGS64_64)-1)/2)
static struct {
	struct REPARSE_DATA_BUFFER_HEADER header;
	struct MOUNTPOINT_REPARSE_BUFFER_HEADER mount_header;
	WCHAR buffer[PATH_PROGS64_64_LENGTH+1+4];
	WCHAR buffer2[PATH_PROGS64_64_LENGTH+1];
} link_to_progs64_64 = {
	{
		IO_REPARSE_TAG_MOUNT_POINT,
		sizeof(struct MOUNTPOINT_REPARSE_BUFFER_HEADER) + (PATH_PROGS64_64_LENGTH+4+1)*2 + (PATH_PROGS64_64_LENGTH+1)*2,
		0
	},
	{
		0, (PATH_PROGS64_64_LENGTH+4)*2, (PATH_PROGS64_64_LENGTH+1+4)*2, PATH_PROGS64_64_LENGTH*2
	},
	L"\\??\\" PATH_PROGS64_64,
	PATH_PROGS64_64
};

#define PATH_PROGS64_32 DRIVE_LETTER L":\\WINXP64\\Progs32"
#define PATH_PROGS64_32_LENGTH ((sizeof(PATH_PROGS64_32)-1)/2)
static struct {
	struct REPARSE_DATA_BUFFER_HEADER header;
	struct MOUNTPOINT_REPARSE_BUFFER_HEADER mount_header;
	WCHAR buffer[PATH_PROGS64_32_LENGTH+1+4];
	WCHAR buffer2[PATH_PROGS64_32_LENGTH+1];
} link_to_progs64_32 = {
	{
		IO_REPARSE_TAG_MOUNT_POINT,
		sizeof(struct MOUNTPOINT_REPARSE_BUFFER_HEADER) + (PATH_PROGS64_32_LENGTH+4+1)*2 + (PATH_PROGS64_32_LENGTH+1)*2,
		0
	},
	{
		0, (PATH_PROGS64_32_LENGTH+4)*2, (PATH_PROGS64_32_LENGTH+1+4)*2, PATH_PROGS64_32_LENGTH*2
	},
	L"\\??\\" PATH_PROGS64_32,
	PATH_PROGS64_32
};

#endif


static WCHAR programfiles_name[] = L"\\??\\" DRIVE_LETTER L":\\Program Files";
static UNICODE_STRING programfiles_str = {
	sizeof(programfiles_name) - sizeof(WCHAR),
	sizeof(programfiles_name) - sizeof(WCHAR),
	programfiles_name
};
static OBJECT_ATTRIBUTES programfiles_obj = {
	sizeof(OBJECT_ATTRIBUTES), NULL,
	&programfiles_str, 0x00000040, //OBJ_CASE_INSENSITIVE
	NULL, NULL
};

static WCHAR programfiles86_name[] = L"\\??\\" DRIVE_LETTER L":\\Program Files (x86)";
static UNICODE_STRING programfiles86_str = {
	sizeof(programfiles86_name) - sizeof(WCHAR),
	sizeof(programfiles86_name) - sizeof(WCHAR),
	programfiles86_name
};
static OBJECT_ATTRIBUTES programfiles86_obj = {
	sizeof(OBJECT_ATTRIBUTES), NULL,
	&programfiles86_str, 0x00000040, //OBJ_CASE_INSENSITIVE
	NULL, NULL
};


int entry(void) {
  int error = 0;
  union {
	REPARSE_DATA_BUFFER data;
	char _alloc[512];
  } target;
	//create_dir(&programfiles_obj);
	//create_link(&programfiles_obj, &link_target, sizeof(link_target));
	//read_link(&programfiles_obj, &target.data, sizeof(target));
	//delete_file(&programfiles_obj);

#if TARGET==32
	/* variant with deleting "Program Files (x86)"
	if (STATUS_SUCCESS == read_link(&programfiles_obj, &target.data, sizeof(target))) {
		if (!are_links_equal((PREPARSE_DATA_BUFFER)&link_to_progs32, &target.data)) {
			if (STATUS_SUCCESS != set_link(&programfiles_obj, &link_to_progs32, sizeof(link_to_progs32))) {
				error = 1;
			}
			delete_file(&programfiles86_obj);
		}
	} else {
		if (STATUS_SUCCESS != create_link(&programfiles_obj, &link_to_progs32, sizeof(link_to_progs32))) {
			error = 1;
		}
	}
	*/
	if (STATUS_SUCCESS == read_link(&programfiles_obj, &target.data, sizeof(target))) {
		if (!are_links_equal((PREPARSE_DATA_BUFFER)&link_to_progs32, &target.data)) {
			if (STATUS_SUCCESS != set_link(&programfiles_obj, &link_to_progs32, sizeof(link_to_progs32))) {
				error = 1;
			}
		}
	} else {
		if (STATUS_SUCCESS != create_link(&programfiles_obj, &link_to_progs32, sizeof(link_to_progs32))) {
			error = 1;
		}
	}

	if (STATUS_SUCCESS == read_link(&programfiles86_obj, &target.data, sizeof(target))) {
		if (!are_links_equal((PREPARSE_DATA_BUFFER)&link_to_progs32, &target.data)) {
			if (STATUS_SUCCESS != set_link(&programfiles86_obj, &link_to_progs32, sizeof(link_to_progs32))) {
				error = 1;
			}
		}
	}
#elif TARGET==64
	if (STATUS_SUCCESS == read_link(&programfiles_obj, &target.data, sizeof(target))) {
		if (!are_links_equal((PREPARSE_DATA_BUFFER)&link_to_progs64_64, &target.data)) {
			if (STATUS_SUCCESS != set_link(&programfiles_obj, &link_to_progs64_64, sizeof(link_to_progs64_64))) {
				error = 1;
			}
		}
	} else {
		if (STATUS_SUCCESS != create_link(&programfiles_obj, &link_to_progs64_64, sizeof(link_to_progs64_64))) {
			error = 1;
		}
	}

	if (STATUS_SUCCESS == read_link(&programfiles86_obj, &target.data, sizeof(target))) {
		if (!are_links_equal((PREPARSE_DATA_BUFFER)&link_to_progs64_32, &target.data)) {
			if (STATUS_SUCCESS != set_link(&programfiles86_obj, &link_to_progs64_32, sizeof(link_to_progs64_32))) {
				error = 1;
			}
		}
	} else {
		if (STATUS_SUCCESS != create_link(&programfiles86_obj, &link_to_progs64_32, sizeof(link_to_progs64_32))) {
			error = 1;
		}
	}
#endif

	return error;
}

