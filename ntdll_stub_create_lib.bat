rem IX86/X86, X64

@echo off
cl /nologo /c /Od ntdll_stub.c /Fo"ntdll_stub.obj"
if /I "%1"=="AMD64" (
	link /nologo /nodefaultlib /dll /machine:%1 /out:ntdll.dll ntdll_stub.obj
) else (
	link /nologo /nodefaultlib /def:ntdll_stub.def /dll /machine:%1 /out:ntdll.dll ntdll_stub.obj
)
del ntdll_stub.obj ntdll.exp ntdll.dll
