@echo off
setlocal

:: path to linkd.exe
set PATH=G:\xp-xp64-dualboot\native;%PATH%

set my_prog32=lnprog32.exe
set my_prog64=lnprog64.exe
set root=T:

goto :main

:cleanup
	rd "%root%\Program Files"
	rd "%root%\Program Files (x86)"
exit /b 0

:cleanup_all
	call :cleanup
	rd %root%\WINXP32\PROGS
	rd %root%\WINXP32
	rd %root%\WINXP64\PROGS64
	rd %root%\WINXP64\PROGS32
	rd %root%\WINXP64
exit /b 0

:create_win32_win64
	setlocal
	set err=0
	md %root%\WINXP32 || set err=1
	md %root%\WINXP32\PROGS || set err=1
	md %root%\WINXP64 || set err=1
	md %root%\WINXP64\PROGS64 || set err=1
	md %root%\WINXP64\PROGS32 || set err=1
exit /b %err%

:link_win32
	linkd "%root%\Program Files" %root%\WINXP32\PROGS > nul
exit /b

:link_win64
	setlocal
	set err=0
	linkd "%root%\Program Files" %root%\WINXP64\PROGS64 > nul || set err=1
	linkd "%root%\Program Files (x86)" %root%\WINXP64\PROGS32 > nul || set err=1
exit /b %err%

:is_linked_to
	setlocal
	for /f "skip=1 tokens=*" %%l in ('linkd %1') do (
		set target="%%l"
	)
	if /i "%target%"=="%2" exit /b 0
exit /b 1

:check_win32
	call :is_linked_to "%root%\Program Files" "%root%\WINXP32\PROGS" || exit /b 1
	if exist "%root%\Program Files (x86)" exit /b 1
exit /b 0

:check_win32_after64
	call :is_linked_to "%root%\Program Files" "%root%\WINXP32\PROGS" || exit /b 1
	call :is_linked_to "%root%\Program Files (x86)" "%root%\WINXP32\PROGS" || exit /b 1
exit /b 0

:check_win64
	call :is_linked_to "%root%\Program Files" "%root%\WINXP64\PROGS64" || exit /b 1
	call :is_linked_to "%root%\Program Files (x86)" "%root%\WINXP64\PROGS32" || exit /b 1
exit /b 0


:test__canceled
	echo CANCELED
exit /b 1

:test__failed
	echo FAILED
exit /b 1

:test__check_failed
	echo CHECK FAILED
exit /b 1



:test__wasNONE_now32
	echo %0
	"%my_prog32%" || goto :test__failed
	call :check_win32 || goto :test__check_failed
	call :cleanup 2>nul
exit /b 0

:test__was32_now32
	echo %0
	call :link_win32 || goto :test__canceled
	"%my_prog32%" || goto :test__failed
	call :check_win32 || goto :test__check_failed
	call :cleanup 2>nul
exit /b 0

:test__was64_now32
	echo %0
	call :link_win64 || goto :test__canceled
	"%my_prog32%" || goto :test__failed
	:: variant with deleting "Program Files (x86)"
	::call :check_win32 || goto :test__check_failed
	call :check_win32_after64 || goto :test__check_failed
	call :cleanup 2>nul
exit /b 0

:test__wasNONE_now64
	echo %0
	"%my_prog64%" || goto :test__failed
	call :check_win64 || goto :test__check_failed
	call :cleanup 2>nul
exit /b 0

:test__was32_now64
	echo %0
	call :link_win32 || goto :test__canceled
	"%my_prog64%" || goto :test__failed
	call :check_win64 || goto :test__check_failed
	call :cleanup 2>nul
exit /b 0

:test__was64_now64
	echo %0
	call :link_win64 || goto :test__canceled
	"%my_prog64%" || goto :test__failed
	call :check_win64 || goto :test__check_failed
	call :cleanup 2>nul
exit /b 0



:main
	set err=0
	call :create_win32_win64 || exit /b 2

	call :test__wasNONE_now32 || set err=1
	call :test__was32_now32 || set err=1
	call :test__was64_now32 || set err=1

	call :test__wasNONE_now64 || set err=1
	call :test__was32_now64 || set err=1
	call :test__was64_now64 || set err=1

	call :cleanup_all 2>nul || set err=2

exit /b %err%
